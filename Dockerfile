FROM registry.gitlab.com/natural-solutions/tippecanoe AS builder
LABEL maintainer="Javier Blanco <javier_blanco@natural-solutions.eu>"

USER tippecanoe
WORKDIR /home/tippecanoe

# Download arbres.geojson
RUN curl -L -s https://www.data.gouv.fr/fr/datasets/r/aaaddd02-206f-4d60-a04c-9a201297a3da > arbres.csv

# Generate tiles
RUN tippecanoe --output-to-directory tiles \
               --force \
               --no-tile-compression \
               --maximum-zoom=g \
               --drop-densest-as-needed \
               --extend-zooms-if-still-dropping \
               arbres.csv

FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /home/tippecanoe/tiles /usr/share/nginx/html/tiles
ADD index.html /usr/share/nginx/html
ADD script.js /usr/share/nginx/html
ADD nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
