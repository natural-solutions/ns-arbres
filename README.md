![logo](./assets/logo.svg)

![hero](./assets/hero.png)

<p align="center">
  <a href="#demo">Demo</a>
  &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#doker-notes">Docker notes</a>
</p>

## Demo

Use this link to test EcoTeka directly in your browser: [https://natural-solutions.gitlab.io/ns-arbres/](https://natural-solutions.gitlab.io/ns-arbres/)

## Docker notes

If docker is installed you can build an image and run this as a container.

```bash
docker build -t ns-ecoteka .
```

Once the image is build, EcoTeka can be invoked by running the following:

```bash
docker run --rm -p 8000:80 ns-ecoteka
```

The optional `--rm` flag removes the container filesystem on completion to prevent cruft build-up. See: https://docs.docker.com/engine/reference/run/#clean-up---rm