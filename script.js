var tilesOSM = [
  "https://a.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
  "https://b.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
  "https://c.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
]
var tilesArbres = [
  `https://natural-solutions.gitlab.io/ns-arbres/tiles/{z}/{x}/{y}.pbf`
]
var map = new mapboxgl.Map({
  center: [
    2.548828,
    46.739861
  ],
  zoom: 5,
  container: 'map',
  style: {
    "bearing": 0,
    "center": [
      0,
      0
    ],
    "glyphs": "https://api.maptiler.com/fonts/{fontstack}/{range}.pbf?key=1zul9rM9yDm3Dbdl3U9r",
    "id": "basic",
    "layers": [
      {
        "id": "background",
        "paint": {
          "background-color": "rgb(39, 39, 39)"
        },
        "type": "background"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "in",
            "class",
            "residential",
            "suburb",
            "neighbourhood"
          ]
        ],
        "id": "landuse_residential",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "fill-color": "rgb(34, 34, 34)",
          "fill-opacity": 0.7
        },
        "source": "openmaptiles",
        "source-layer": "landuse",
        "type": "fill"
      },
      {
        "filter": [
          "==",
          "class",
          "grass"
        ],
        "id": "landcover_grass",
        "paint": {
          "fill-color": "rgb(157, 185, 157)",
          "fill-opacity": 0.4
        },
        "source": "openmaptiles",
        "source-layer": "landcover",
        "type": "fill"
      },
      {
        "filter": [
          "==",
          "class",
          "wood"
        ],
        "id": "landcover_wood",
        "paint": {
          "fill-color": "rgb(143, 189, 138)",
          "fill-opacity": {
            "base": 1,
            "stops": [
              [
                8,
                0.6
              ],
              [
                22,
                1
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "landcover",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "!=",
            "intermittent",
            1
          ],
          [
            "!=",
            "brunnel",
            "tunnel"
          ]
        ],
        "id": "water",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "fill-color": "rgb(0, 0, 0)"
        },
        "source": "openmaptiles",
        "source-layer": "water",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "==",
            "intermittent",
            1
          ]
        ],
        "id": "water_intermittent",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "fill-color": "rgb(0, 0, 0)",
          "fill-opacity": 0.7
        },
        "source": "openmaptiles",
        "source-layer": "water",
        "type": "fill"
      },
      {
        "filter": [
          "==",
          "subclass",
          "ice_shelf"
        ],
        "id": "landcover_ice-shelf",
        "layout": {
          "visibility": "none"
        },
        "paint": {
          "fill-color": "hsl(47, 26%, 88%)",
          "fill-opacity": 0.8
        },
        "source": "openmaptiles",
        "source-layer": "landcover",
        "type": "fill"
      },
      {
        "filter": [
          "==",
          "subclass",
          "glacier"
        ],
        "id": "landcover_glacier",
        "layout": {
          "visibility": "none"
        },
        "paint": {
          "fill-color": "hsl(47, 22%, 94%)",
          "fill-opacity": {
            "base": 1,
            "stops": [
              [
                0,
                1
              ],
              [
                8,
                0.5
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "landcover",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "in",
            "class",
            "sand"
          ]
        ],
        "id": "landcover_sand",
        "metadata": {

        },
        "paint": {
          "fill-antialias": false,
          "fill-color": "rgb(110, 188, 42)",
          "fill-opacity": 0.3
        },
        "source": "openmaptiles",
        "source-layer": "landcover",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "tunnel"
          ]
        ],
        "id": "waterway_tunnel",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-dasharray": [
            3,
            3
          ],
          "line-gap-width": {
            "stops": [
              [
                12,
                0
              ],
              [
                20,
                6
              ]
            ]
          },
          "line-opacity": 1,
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                8,
                1
              ],
              [
                20,
                2
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "waterway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "!in",
            "brunnel",
            "tunnel",
            "bridge"
          ],
          [
            "!=",
            "intermittent",
            1
          ]
        ],
        "id": "waterway",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-opacity": 1,
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                8,
                1
              ],
              [
                20,
                8
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "waterway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "!in",
            "brunnel",
            "tunnel",
            "bridge"
          ],
          [
            "==",
            "intermittent",
            1
          ]
        ],
        "id": "waterway_intermittent",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-dasharray": [
            2,
            1
          ],
          "line-opacity": 1,
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                8,
                1
              ],
              [
                20,
                8
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "waterway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "tunnel"
          ],
          [
            "==",
            "class",
            "transit"
          ]
        ],
        "id": "tunnel_railway_transit",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "minzoom": 0,
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-dasharray": [
            3,
            3
          ],
          "line-opacity": {
            "base": 1,
            "stops": [
              [
                11,
                0
              ],
              [
                16,
                1
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "id": "building",
        "paint": {
          "fill-antialias": true,
          "fill-color": "rgba(222, 211, 190, 1)",
          "fill-opacity": {
            "base": 1,
            "stops": [
              [
                13,
                0
              ],
              [
                15,
                1
              ]
            ]
          },
          "fill-outline-color": {
            "stops": [
              [
                15,
                "rgba(212, 177, 146, 0)"
              ],
              [
                16,
                "rgba(212, 177, 146, 0.5)"
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "building",
        "type": "fill",
        "layout": {
          "visibility": "none"
        }
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "==",
            "class",
            "pier"
          ]
        ],
        "id": "road_area_pier",
        "layout": {
          "visibility": "visible"
        },
        "metadata": {

        },
        "paint": {
          "fill-antialias": true,
          "fill-color": "rgb(39, 39, 39)"
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "in",
            "class",
            "pier"
          ]
        ],
        "id": "road_pier",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "metadata": {

        },
        "paint": {
          "line-color": "rgb(24, 21, 14)",
          "line-width": {
            "base": 1.2,
            "stops": [
              [
                15,
                1
              ],
              [
                17,
                4
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "in",
            "brunnel",
            "bridge"
          ]
        ],
        "id": "road_bridge_area",
        "layout": {

        },
        "paint": {
          "fill-color": "rgb(39, 39, 39)",
          "fill-opacity": 0.5
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "in",
            "class",
            "path",
            "track"
          ]
        ],
        "id": "road_path",
        "layout": {
          "line-cap": "square",
          "line-join": "bevel"
        },
        "paint": {
          "line-color": "rgb(42, 42, 42)",
          "line-dasharray": [
            1,
            1
          ],
          "line-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                7
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "in",
            "class",
            "minor",
            "service"
          ]
        ],
        "id": "road_minor",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "minzoom": 13,
        "paint": {
          "line-color": "rgb(42, 42, 42)",
          "line-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "tunnel"
          ],
          [
            "==",
            "class",
            "minor_road"
          ]
        ],
        "id": "tunnel_minor",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "paint": {
          "line-color": "rgb(34, 34, 34)",
          "line-dasharray": [
            0.36,
            0.18
          ],
          "line-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "tunnel"
          ],
          [
            "in",
            "class",
            "primary",
            "secondary",
            "tertiary",
            "trunk"
          ]
        ],
        "id": "tunnel_major",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-dasharray": [
            0.28,
            0.14
          ],
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                6,
                0.5
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Polygon"
          ],
          [
            "in",
            "class",
            "runway",
            "taxiway"
          ]
        ],
        "id": "aeroway_area",
        "layout": {
          "visibility": "visible"
        },
        "metadata": {
          "mapbox:group": "1444849345966.4436"
        },
        "minzoom": 4,
        "paint": {
          "fill-color": "rgba(255, 255, 255, 1)",
          "fill-opacity": {
            "base": 1,
            "stops": [
              [
                13,
                0
              ],
              [
                14,
                1
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "aeroway",
        "type": "fill"
      },
      {
        "filter": [
          "all",
          [
            "in",
            "class",
            "taxiway"
          ],
          [
            "==",
            "$type",
            "LineString"
          ]
        ],
        "id": "aeroway_taxiway",
        "layout": {
          "line-cap": "round",
          "line-join": "round",
          "visibility": "visible"
        },
        "metadata": {
          "mapbox:group": "1444849345966.4436"
        },
        "minzoom": 12,
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-opacity": 1,
          "line-width": {
            "base": 1.5,
            "stops": [
              [
                12,
                1
              ],
              [
                17,
                10
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "aeroway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "in",
            "class",
            "runway"
          ],
          [
            "==",
            "$type",
            "LineString"
          ]
        ],
        "id": "aeroway_runway",
        "layout": {
          "line-cap": "round",
          "line-join": "round",
          "visibility": "visible"
        },
        "metadata": {
          "mapbox:group": "1444849345966.4436"
        },
        "minzoom": 4,
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-opacity": 1,
          "line-width": {
            "base": 1.5,
            "stops": [
              [
                11,
                4
              ],
              [
                17,
                50
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "aeroway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "in",
            "class",
            "trunk",
            "primary"
          ]
        ],
        "id": "road_trunk_primary",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                6,
                0.5
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "in",
            "class",
            "secondary",
            "tertiary"
          ]
        ],
        "id": "road_secondary_tertiary",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                6,
                0.5
              ],
              [
                20,
                20
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "class",
            "motorway"
          ]
        ],
        "id": "road_major_motorway",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-offset": 0,
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                8,
                1
              ],
              [
                16,
                10
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "class",
            "transit"
          ],
          [
            "!=",
            "brunnel",
            "tunnel"
          ]
        ],
        "id": "railway_transit",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-opacity": {
            "base": 1,
            "stops": [
              [
                11,
                0
              ],
              [
                16,
                1
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "==",
          "class",
          "rail"
        ],
        "id": "railway",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-opacity": {
            "base": 1,
            "stops": [
              [
                11,
                0
              ],
              [
                16,
                1
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ]
        ],
        "id": "waterway-bridge-case",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "paint": {
          "line-color": "#bbbbbb",
          "line-gap-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          },
          "line-width": {
            "base": 1.6,
            "stops": [
              [
                12,
                0.5
              ],
              [
                20,
                10
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "waterway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ]
        ],
        "id": "waterway-bridge",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "hsl(205, 56%, 73%)",
          "line-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "waterway",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ],
          [
            "==",
            "class",
            "minor_road"
          ]
        ],
        "id": "bridge_minor case",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "paint": {
          "line-color": "rgb(17, 17, 17)",
          "line-gap-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          },
          "line-width": {
            "base": 1.6,
            "stops": [
              [
                12,
                0.5
              ],
              [
                20,
                10
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ],
          [
            "in",
            "class",
            "primary",
            "secondary",
            "tertiary",
            "trunk"
          ]
        ],
        "id": "bridge_major case",
        "layout": {
          "line-cap": "butt",
          "line-join": "miter"
        },
        "paint": {
          "line-color": "rgb(17, 17, 17)",
          "line-gap-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          },
          "line-width": {
            "base": 1.6,
            "stops": [
              [
                12,
                0.5
              ],
              [
                20,
                10
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ],
          [
            "==",
            "class",
            "minor_road"
          ]
        ],
        "id": "bridge_minor",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "rgb(34, 34, 34)",
          "line-width": {
            "base": 1.55,
            "stops": [
              [
                4,
                0.25
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "==",
            "brunnel",
            "bridge"
          ],
          [
            "in",
            "class",
            "primary",
            "secondary",
            "tertiary",
            "trunk"
          ]
        ],
        "id": "bridge_major",
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "rgb(50, 50, 50)",
          "line-width": {
            "base": 1.4,
            "stops": [
              [
                6,
                0.5
              ],
              [
                20,
                30
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "transportation",
        "type": "line"
      },
      {
        "filter": [
          "in",
          "admin_level",
          4,
          6,
          8
        ],
        "id": "admin_sub",
        "layout": {
          "visibility": "visible"
        },
        "paint": {
          "line-color": "rgba(0, 0, 0, 0.5)",
          "line-dasharray": [
            2,
            1
          ]
        },
        "source": "openmaptiles",
        "source-layer": "boundary",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "<=",
            "admin_level",
            2
          ],
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "!has",
            "claimed_by"
          ]
        ],
        "id": "admin_country_z0-4",
        "layout": {
          "line-cap": "round",
          "line-join": "round",
          "visibility": "visible"
        },
        "maxzoom": 5,
        "minzoom": 0,
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-width": {
            "base": 1.3,
            "stops": [
              [
                3,
                0.5
              ],
              [
                22,
                15
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "boundary",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "<=",
            "admin_level",
            2
          ],
          [
            "==",
            "$type",
            "LineString"
          ]
        ],
        "id": "admin_country_z5-",
        "layout": {
          "line-cap": "round",
          "line-join": "round",
          "visibility": "visible"
        },
        "minzoom": 5,
        "paint": {
          "line-color": "rgb(0, 0, 0)",
          "line-width": {
            "base": 1.3,
            "stops": [
              [
                3,
                0.5
              ],
              [
                22,
                15
              ]
            ]
          }
        },
        "source": "openmaptiles",
        "source-layer": "boundary",
        "type": "line"
      },
      {
        "filter": [
          "all",
          [
            "has",
            "iata"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "airport_label",
        "layout": {
          "icon-size": 1,
          "text-anchor": "top",
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 8,
          "text-offset": [
            0,
            0.5
          ],
          "text-size": 11,
          "visibility": "visible"
        },
        "minzoom": 10,
        "paint": {
          "text-color": "rgb(78, 78, 78)",
          "text-halo-blur": 1,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 1
        },
        "source": "openmaptiles",
        "source-layer": "aerodrome_label",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "has",
            "iata"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "airport_label-fr",
        "layout": {
          "icon-size": 1,
          "text-anchor": "top",
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 8,
          "text-offset": [
            0,
            0.5
          ],
          "text-size": 11,
          "visibility": "visible"
        },
        "minzoom": 10,
        "paint": {
          "text-color": "rgb(78, 78, 78)",
          "text-halo-blur": 1,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 1
        },
        "source": "openmaptiles",
        "source-layer": "aerodrome_label",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "road_major_label",
        "layout": {
          "symbol-placement": "line",
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-letter-spacing": 0.1,
          "text-rotation-alignment": "map",
          "text-size": {
            "base": 1.4,
            "stops": [
              [
                10,
                8
              ],
              [
                20,
                14
              ]
            ]
          },
          "text-transform": "uppercase",
          "visibility": "visible"
        },
        "minzoom": 13,
        "paint": {
          "text-color": "rgb(180, 180, 180)",
          "text-halo-color": "rgb(0, 0, 0)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "transportation_name",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "LineString"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "road_major_label-fr",
        "layout": {
          "symbol-placement": "line",
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-letter-spacing": 0.1,
          "text-rotation-alignment": "map",
          "text-size": {
            "base": 1.4,
            "stops": [
              [
                10,
                8
              ],
              [
                20,
                14
              ]
            ]
          },
          "text-transform": "uppercase",
          "visibility": "visible"
        },
        "minzoom": 13,
        "paint": {
          "text-color": "rgb(180, 180, 180)",
          "text-halo-color": "rgb(0, 0, 0)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "transportation_name",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "!in",
            "class",
            "city",
            "state",
            "country",
            "continent"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "place_label_other",
        "layout": {
          "text-anchor": "center",
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 6,
          "text-size": {
            "stops": [
              [
                6,
                10
              ],
              [
                12,
                14
              ]
            ]
          },
          "visibility": "visible"
        },
        "minzoom": 8,
        "paint": {
          "text-color": "rgb(116, 116, 116)",
          "text-halo-blur": 0,
          "text-halo-color": "rgb(0, 0, 0)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "!in",
            "class",
            "city",
            "state",
            "country",
            "continent"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "place_label_other-fr",
        "layout": {
          "text-anchor": "center",
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 6,
          "text-size": {
            "stops": [
              [
                6,
                10
              ],
              [
                12,
                14
              ]
            ]
          },
          "visibility": "visible"
        },
        "minzoom": 8,
        "paint": {
          "text-color": "rgb(116, 116, 116)",
          "text-halo-blur": 0,
          "text-halo-color": "rgb(0, 0, 0)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "city"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "place_label_city",
        "layout": {
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                11
              ],
              [
                8,
                16
              ]
            ]
          }
        },
        "maxzoom": 16,
        "paint": {
          "text-color": "rgb(180, 180, 180)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "city"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "place_label_city-fr",
        "layout": {
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                11
              ],
              [
                8,
                16
              ]
            ]
          }
        },
        "maxzoom": 16,
        "paint": {
          "text-color": "rgb(180, 180, 180)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "country"
          ],
          [
            "!has",
            "iso_a2"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "country_label-other",
        "layout": {
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                12
              ],
              [
                8,
                22
              ]
            ]
          },
          "visibility": "visible"
        },
        "maxzoom": 12,
        "paint": {
          "text-color": "rgb(147, 147, 147)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "country"
          ],
          [
            "!has",
            "iso_a2"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "country_label-other-fr",
        "layout": {
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Regular"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                12
              ],
              [
                8,
                22
              ]
            ]
          },
          "visibility": "visible"
        },
        "maxzoom": 12,
        "paint": {
          "text-color": "rgb(147, 147, 147)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "country"
          ],
          [
            "has",
            "iso_a2"
          ],
          [
            "!has",
            "name:fr"
          ]
        ],
        "id": "country_label",
        "layout": {
          "text-field": "{name:latin}",
          "text-font": [
            "Noto Sans Bold"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                12
              ],
              [
                8,
                22
              ]
            ]
          },
          "visibility": "visible"
        },
        "maxzoom": 12,
        "paint": {
          "text-color": "rgb(147, 147, 147)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "filter": [
          "all",
          [
            "==",
            "$type",
            "Point"
          ],
          [
            "==",
            "class",
            "country"
          ],
          [
            "has",
            "iso_a2"
          ],
          [
            "has",
            "name:fr"
          ]
        ],
        "id": "country_label-fr",
        "layout": {
          "text-field": "{name:fr}",
          "text-font": [
            "Noto Sans Bold"
          ],
          "text-max-width": 10,
          "text-size": {
            "stops": [
              [
                3,
                12
              ],
              [
                8,
                22
              ]
            ]
          },
          "visibility": "visible"
        },
        "maxzoom": 12,
        "paint": {
          "text-color": "rgb(147, 147, 147)",
          "text-halo-blur": 0,
          "text-halo-color": "rgba(0, 0, 0, 0.75)",
          "text-halo-width": 2
        },
        "source": "openmaptiles",
        "source-layer": "place",
        "type": "symbol"
      },
      {
        "id": "arbres",
        "type": "circle",
        "source": "arbres",
        "source-layer": "arbres",
        'paint': {
          'circle-radius': {
            'base': 1.75,
            'stops': [[12, 2], [22, 180]]
          },
          'circle-color': [
            'match',
            ['get', 'genre_latin'],
            "Quercus",
            "#63b598",
            "Liquidambar",
            "#ce7d78",
            "Fraxinus",
            "#ea9e70",
            "Liriodendron",
            "#a48a9e",
            "Acer",
            "#c6e1e8",
            "Prunus",
            "#648177",
            "Platanus",
            "#0d5ac1",
            "Malus",
            "#f205e6",
            "Tilia",
            "#1c0365",
            "Ficus",
            "#14a9ad",
            "Alnus",
            "#4ca2f9",
            "Pinus",
            "#a4e43f",
            "Populus",
            "#d298e2",
            "Castanea",
            "#6119d0",
            "Fagus",
            "#d2737d",
            "Catalpa",
            "#c0a43c",
            "Carpinus",
            "#f2510e",
            "Betula",
            "#651be6",
            "Lagerstroemia",
            "#79806e",
            "Acacia",
            "#61da5e",
            "Salix",
            "#cd2f00",
            "Morus",
            "#9348af",
            "Robinia",
            "#01ac53",
            "Albizia",
            "#c5a4fb",
            "Ligustrum",
            "#996635",
            "Magnolia",
            "#b11573",
            "Pyrus",
            "#4bb473",
            "Picea",
            "#75d89e",
            "Chamaerops",
            "#2f3f94",
            "Araucaria",
            "#2f7b99",
            "Celtis",
            "#da967d",
            "Sorbus",
            "#34891f",
            "Chamaecyparis",
            "#b0d87b",
            "Dracaena",
            "#ca4751",
            "Phoenix",
            "#7e50a8",
            "Ulmus",
            "#c4d647",
            "Ginkgo",
            "#e0eeb8",
            "Sophora",
            "#11dec1",
            "Aesculus",
            "#289812",
            "Sequoia",
            "#566ca0",
            "Abies",
            "#ffdbe1",
            "Gleditsia",
            "#2f1179",
            "Cedrus",
            "#935b6d",
            "Cornus",
            "#916988",
            "Corylus",
            "#513d98",
            "Laurus",
            "#aead3a",
            "Cupressus",
            "#9e6d71",
            "Chitalpa",
            "#4b5bdc",
            "Pterocarya",
            "#0cd36d",
            "Koelreuteria",
            "#250662",
            "Taxodium",
            "#cb5bea",
            "Melia",
            "#228916",
            "Ostrya",
            "#ac3e1b",
            "Cupressocypa",
            "#df514a",
            "Cupressocyparis",
            "#539397",
            "Diospiros",
            "#880977",
            "Cedrela",
            "#f697c1",
            "Cercidiphyllum",
            "#ba96ce",
            "Thuja",
            "#679c9d",
            "Juglans",
            "#c6c42c",
            "Cryptomeria",
            "#5d2c52",
            "Calocedrus",
            "#48b41b",
            "Ptelea",
            "#e1cf3b",
            "Hovenia",
            "#5be4f0",
            "Metasequoia",
            "#57c4d8",
            "Amelanchier",
            "#a4d17a",
            "Cercis",
            "#225b8a",
            "Juniperus",
            "#be608b",
            "Paulownia",
            "#96b00c",
            "Washingtonia",
            "#088baf",
            "Olea",
            "#f158bf",
            "Tamarix",
            "#e145ba",
            "Sequoiadendron",
            "#ee91e3",
            "Cydonia",
            "#05d371",
            "Mespilus",
            "#5426e0",
            "Parrotia",
            "#4834d0",
            "Taxus",
            "#802234",
            "Zelkova",
            "#6749e8",
            "Thujopsis",
            "#0971f0",
            "Osmanthus",
            "#8fb413",
            "Gymnocladus",
            "#b2b4f0",
            "Styrax",
            "#c3c89d",
            "Ilex",
            "#c9a941",
            "Laburnum",
            "#41d158",
            "Eucalyptus",
            "#fb21a3",
            "Ailanthus",
            "#51aed9",
            "Crataegus",
            "#5bb32d",
            "Firmiana",
            "#807fba",
            "Diospyros",
            "#21538e",
            "Davidia",
            "#89d534",
            "Nothofagus",
            "#d36647",
            "Phellodendron",
            "#7fb411",
            "Cladastris",
            "#0023b8",
            "Halesia",
            "#3b8c2a",
            "Toona",
            "#986b53",
            "Carya",
            "#f50422",
            "Phillyrea",
            "#983f7a",
            "Viburnum",
            "#ea24a3",
            "Eucommia",
            "#79352c",
            "Broussonetia",
            "#521250",
            "Nyssa",
            "#c79ed2",
            "Cinnamomum",
            "#d6dd92",
            "Tetradium",
            "#e33e52",
            "Sambucus",
            "#b2be57",
            "Cladrastis",
            "#fa06ec",
            "Ehretia",
            "#1bb699",
            "Maclura",
            "#6b2e5f",
            "Trachycarpus",
            "#64820f",
            "Camellia",
            "#1c271a",
            "Sciadopitys",
            "#21538e",
            "Eriobotrya",
            "#89d534",
            "Arbutus",
            "#d36647",
            "Heptacodium",
            "#7fb411",
            "Wisteria",
            "#0023b8",
            "Vitex",
            "#3b8c2a",
            "Asimina",
            "#986b53",
            "Syringa",
            "#f50422",
            "Parrotiopsis",
            "#983f7a",
            "Erythrina",
            "#ea24a3",
            "Euptelea",
            "#79352c",
            "Sassafras",
            "#521250",
            "Larix",
            "#c79ed2",
            "Podocarpus",
            "#d6dd92",
            "Platycladus",
            "#e33e52",
            "Cephalotaxus",
            "#b2be57",
            "Pseudotsuga",
            "#fa06ec",
            "Acca",
            "#1bb699",
            "Sabal",
            "#6b2e5f",
            "Jacaranda",
            "#64820f",
            "Butia",
            "#1c271b",
            "Aphananthe",
            "#9cb64a",
            "Phyllostachys",
            "#996c48",
            "Allocasuarina",
            "#9ab9b7",
            "Pseudosasa",
            "#06e052",
            "Chimonanthus",
            "#e3a481",
            "Calycanthus",
            "#0eb621",
            "Persea",
            "#fc458e",
            "Chimonobambusa",
            "#b2db15",
            "Bauhinia",
            "#aa226d",
            "Euonymus",
            "#792ed8",
            "Lindera",
            "#73872a",
            "Petteria",
            "#520d3a",
            "Wollemia",
            "#cefcb8",
            "Photinia",
            "#a5b3d9",
            "Bourreria",
            "#7d1d85",
            "Aralia",
            "#c4fd57",
            "Fontanesia",
            "#f1ae16",
            "Escallonia",
            "#8fe22a",
            "Bumelia",
            "#ef6e3c",
            "Buxus",
            "#243eeb",
            "Actinidia",
            "#1dc18c",
            "Idesia",
            "#dd93fd",
            "Poliothyrsis",
            "#3f8473",
            "Cudrania",
            "#e7dbce",
            "Cotinus",
            "#421f79",
            "Schinus",
            "#7a3d93",
            "Sapindus",
            "#635f6d",
            "Rhus",
            "#93f2d7",
            "Pittosporum",
            "#9b5c2a",
            "Poncirus",
            "#15b9ee",
            "Manihot",
            "#0f5997",
            "Zanthoxylum",
            "#409188",
            "Citrus",
            "#911e20",
            "Orixa",
            "#1350ce",
            "Pistacia",
            "#10e5b1",
            "Ziziphus",
            "#fff4d7",
            "Elaeagnus",
            "#cb2582",
            "Armeniaca",
            "#ce00be",
            "Hippophae",
            "#32d5d6",
            "Cunninghamia",
            "#17232a",
            "Pirus",
            "#608572",
            "Evodia",
            "#c79bc2",
            "Semiarundinaria",
            "#00f87c",
            "Hibiscus",
            "#77772a",
            "Clerodendron",
            "#6995ba",
            "Libocedrus",
            "#fc6b57",
            "Pteroceltis",
            "#f07815",
            "Eleagnus",
            "#8fd883",
            "Lonicera",
            "#060e27",
            "Chionanthus",
            "#96e591",
            "Sterculia",
            "#21d52e",
            "Pterostyrax",
            "#d00043",
            "Rhamnus",
            "#b47162",
            "Staphylea",
            "#1ec227",
            "Punica",
            "#4f0f6f",
            "Sorbopyrus",
            "#1d1d58",
            "Poncinos",
            "#947002",
            "Gincko",
            "#bde052",
            "Gingko",
            "#e08c56",
            "Euodia",
            "#28fcfd",
            "Juglan",
            "#bb09bb",
            "Fruitier",
            "#36486a",
            "Tialia",
            "#d02e29",
            "Epicea",
            "#1ae6db",
            "Cotoneaster",
            "#3e464c",
            "Deutzia",
            "#a84a8f",
            "Liriodentron",
            "#911e7e",
            "Mesplus",
            "#3f16d9",
            "X Cupressocyparis",
            "#0f525f",
            "Alangium",
            "#ac7c0a",
            "Callistemon",
            "#b4c086",
            "Crataegomespilus",
            "#c9d730",
            "Clerodendrum",
            "#30cc49",
            "Torreya",
            "#3d6751",
            "Tsuga",
            "#fb4c03",
            "Garrya",
            "#640fc1",
            "Aronia",
            "#62c03e",
            "Hemiptelea",
            "#d3493a",
            "X Chitalpa",
            "#88aa0b",
            "Laburnocytisus",
            "#406df9",
            "Rhododendron",
            "#615af0",
            "Luma",
            "#4be47a",
            "Abelia",
            "#2a3434",
            "Tapiscia",
            "#4a543f",
            "Brachychiton",
            "#79bca0",
            "Genista",
            "#a8b8d4",
            "Fremontodendron",
            "#00efd4",
            "Distylium",
            "#7ad236",
            "Maackia",
            "#7260d8",
            "X Sorbopyrus",
            "#1deaa7",
            "Stewartia",
            "#06f43a",
            "Platycarya",
            "#823c59",
            "Enkianthus",
            "#e3d94c",
            "Phyllanthus",
            "#dc1c06",
            "Philadelphus",
            "#f53b2a",
            "Euscaphis",
            "#b46238",
            "Brahea",
            "#2dfff6",
            "Carica",
            "#a82b89",
            "Xanthoceras",
            "#1a8011",
            "Kalopanax",
            "#436a9f",
            "Caragana",
            "#1a806a",
            "Cordyline",
            "#4cf09d",
            "Sycopsis",
            "#c188a2",
            "Prunus Maackii Amber Beauty",
            "#67eb4b",
            "Tilia Tomentosa",
            "#b308d3",
            "Prunus Pandora",
            "#fc7e41",
            "Ostria",
            "#af3101",
            "Betulus",
            "#ff065a",
            "Azara",
            "#71b1f4",
            "Staphyllea",
            "#a2f8a5",
            "Quercus Robur",
            "#e23dd0",
            "Sequioadendron",
            "#d3486d",
            "Meleze",
            "#00f7f9",
            "Sureau",
            "#474893",
            "Malus Toringo Sargentii",
            "#3cec35",
            "Nerium",
            "#1c65cb",
            "Zizyphus",
            "#5d1d0c",
            "Stercularia",
            "#2d7d2a",
            "Erythea",
            "#ff3420",
            "Jubaea",
            "#5cdd87",
            "Copernicia",
            "#a259a4",
            "Chorisia",
            "#e4ac44",
            "Arecastrum",
            "#1bede6",
            "Arbustus",
            "#8798a4",
            "Sambuscus",
            "#d7790f",
            "Amelanchus",
            "#b2c24f",
            "Elaegnus",
            "#de73c2",
            "Buddleja",
            "#d70a9c",
            "Quercus Rubra",
            "#25b6a7",
            "Comus",
            "#88e9b8",
            "Firmania",
            "#c2b0e2",
            "Cerisier A Fruits",
            "#86e98f",
            "Tamaris",
            "#ae90e2",
            "Eucomia",
            "#1a806b",
            "Castanea ?",
            "#436a9e",
            "Lagestromia",
            "#0ec0ff",
            "Taiwania",
            "#f812b3",
            "Sapium",
            "#b17fc9",
            "Cladatris",
            "#8d6c2f",
            "Sup",
            "#d3277a",
            "Cynodia",
            "#2ca1ae",
            "Xcupressocyparis",
            "#9685eb",
            "Rosa",
            "#8a96c6",
            "Pyracantha",
            "#dba2e6",
            "Phyllirea",
            "#76fc1b",
            "Prrunus",
            "#608fa4",
            "Lagertroemia",
            "#20f6ba",
            "Castenea",
            "#07d7f6",
            '#fff'
          ]
        }
      }
    ],
    "metadata": {
      "maptiler:copyright": "This style was generated on MapTiler Cloud. Usage outside of MapTiler Cloud requires valid OpenMapTiles Production Package: https://openmaptiles.com/production-package/ -- please contact us.",
      "openmaptiles:version": "3.x"
    },
    "name": "Basic",
    "pitch": 0,
    "sources": {
      "openmaptiles": {
        "type": "vector",
        "url": "https://api.maptiler.com/tiles/v3/tiles.json?key=1zul9rM9yDm3Dbdl3U9r"
      },
      "arbres": {
        "type": "vector",
        "tiles": tilesArbres,
        "minzoom": 0,
        "maxzoom": 13
      }
    },
    "version": 8,
    "zoom": 1
  }
});

  async function getInfoFrowWikipedia(prop, genre) {
  const url = `https://en.wikipedia.org/w/api.php?action=query&prop=${prop}&titles=${genre}&rvslots=*&rvprop=content&formatversion=2&origin=*&format=json`
  const response = await fetch(url)
  const json = await response.json()

  if (prop === 'extracts' && json.hasOwnProperty('query')) {
    return json.query.pages.pop().extract
  }
  
  if (prop === 'pageimages' && json.hasOwnProperty('query')) {
    const width = document.getElementsByClassName('sidebar-content')[0].offsetWidth
    return json.query.pages.pop().thumbnail.source.replace(/\/\d\dpx-/, `/${width}px-`)
  }
}


map.on('click', 'arbres', function (e) {
  var bbox = [
    [e.point.x - 5, e.point.y - 5],
    [e.point.x + 5, e.point.y + 5]
  ];

  var features = map.queryRenderedFeatures(bbox, {
    layers: ['arbres']
  });

  if (features.length) {
    const feature = features.pop()
    const genre = feature.properties.genre_latin
      .toLowerCase().replace(' ', '_')

    getInfoFrowWikipedia('pageimages', genre).then((src) => {
      document.getElementById('image_wiki_content').src = src || null
    })

    getInfoFrowWikipedia('extracts', genre).then((content) => {
      document.getElementById('wiki_content').innerHTML = content || null
    })
  }

});
